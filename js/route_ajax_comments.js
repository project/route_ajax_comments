(function($, Drupal) {
  
  $.fn.showMoreComments = function(data) {
    
    $([document.documentElement, document.body]).animate({
        scrollTop: $(data).offset().top
    }, 1000);
    
  };
  
})(jQuery, Drupal);



