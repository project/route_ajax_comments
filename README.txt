ROUTE AJAX COMMENTS DRUPAL MODULE
=================================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Usage
 * FAQ
 * Maintainers


INTRODUCTION
------------

This module provides a simple comments widget allowing site users to post comments filtering by Route or URL.

FEATURES
--------

 * comments filtered by Drupal Route or site relative url.
 * AJAX system.
 * Define number of comments to show per page.
 * Allow anonymous comments and define multiple publishing policies.
 * pending approval / visible status
 * Comments handled as Entities


REQUIREMENTS
------------

Required Core modules:

 * options
 * user
 * images


INSTALLATION
------------

Install as usual.


CONFIGURATION
-------------

Settings form : /admin/routecomment/route_comments_settings

You will find more tabs on that route to customize the route_comment_entity.


USAGE
-----

Once you have installed Route Ajax Comments module, you'll be able to add the comments 
widget to any content in two ways:

- Placing the Route Ajax Comments Block, in the same way you do with another Drupal Block.

- Using in any of your customs twig templates.

  {{ route_ajax_comments() }}

Be aware if you use the second way in a custom module, you'll have to include the Route Ajax Comments dependency in your_module.info.yml file, because that function call in a twig template, will raise an error, if Route Ajax Comment module is not enabled.

You can configure some exclusive settings for the block or tpl widget you're trying to insert. These settings refers basically to appearance and simple bahaviour of the block:

- Show more button, for Ajax loading of conversation pages.
- Widget comments filtering on Route or relative url.
- Number of comments to show per conversation page.


FAQ
---

More info at: 

https://redribera.org/development/route-ajax-comments


MAINTAINERS
-----------

 * Julio Laguna (redribera) - https://www.drupal.org/u/redribera