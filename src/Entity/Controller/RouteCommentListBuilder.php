<?php

namespace Drupal\route_ajax_comments\Entity\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for route_comment_entity.
 *
 * @ingroup route_ajax_comments
 */
class RouteCommentListBuilder extends EntityListBuilder {

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager')->getStorage($entity_type->id()),
      $container->get('url_generator')
    );
  }

  /**
   * Constructs a new RouteCommentBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The url generator.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator) {
    parent::__construct($entity_type, $storage);
    $this->urlGenerator = $url_generator;
  }

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Ajax Route Comments implements a Route Comments model. These Route Comments are fieldable entities. You can manage the fields on the <a href="@adminlink">Route Comments admin page</a>.', [
        '@adminlink' => $this->urlGenerator->generateFromRoute('route_ajax_comments.route_comments_settings'),
      ]),
    ];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the Route Comment list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header['id'] = $this->t('Route Comment ID');
    $header['comment_body'] = $this->t('Comment Body');
    $header['route'] = $this->t('Route Name');
    $header['url'] = $this->t('Relative URL');
    $header['host'] = $this->t('Host');
    $header['owner'] = $this->t('Owner');
    $header['created'] = $this->t('Created');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\route_ajax_comments\Entity\RouteComment */
    $row['id'] = $entity->id();
    $row['comment_body'] = $entity->comment_body->value;
    $row['route'] = $entity->route->value;
    $row['url'] = $entity->url->value;
    $row['host'] = $entity->host->value;
    $row['owner'] = $entity->getOwner()->link();
    $row['created'] = \Drupal::service('date.formatter')->format($entity->created->value);
    $row['status'] = $entity->status->value;
    return $row + parent::buildRow($entity);
  }

}
