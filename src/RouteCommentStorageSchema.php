<?php

namespace Drupal\route_ajax_comments;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the RouteComment schema handler.
 */
class RouteCommentStorageSchema extends SqlContentEntityStorageSchema {

  /**
   * {@inheritdoc}
   */
  protected function getEntitySchema(ContentEntityTypeInterface $entity_type, $reset = FALSE) {
    $schema = parent::getEntitySchema($entity_type, $reset);

    if ($data_table = $this->storage->getDataTable()) {
      $schema[$data_table]['indexes'] += [
        'routecomment__route' => ['route'],
        'routecomment__url' => ['url'],
      ];
    }

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  protected function getSharedTableFieldSchema(FieldStorageDefinitionInterface $storage_definition, $table_name, array $column_mapping) {
    $schema = parent::getSharedTableFieldSchema($storage_definition, $table_name, $column_mapping);
    $field_name = $storage_definition->getName();

    if ($table_name == 'route_comment') {
      switch ($field_name) {
        case 'langcode':
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;

        case 'route':
          // Create index over route field.
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;

        case 'url':
          // Create index over url field.
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;

        case 'host':
          // Create index over host field.
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;

        case 'status':
          // Create index over status field.
          $this->addSharedTableFieldIndex($storage_definition, $schema, TRUE);
          break;

      }
    }

    return $schema;
  }

}
