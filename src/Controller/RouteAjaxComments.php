<?php

namespace Drupal\route_ajax_comments\Controller;

use Drupal\Core\Form\FormState;
use Drupal\route_ajax_comments\Entity\RouteComment;
use Drupal\file\Entity\File;

/**
 * Route Ajax Comments Custom Class.
 */
class RouteAjaxComments {
  const SETTINGS = 'route_ajax_comments.settings';

  // Publishing comments methods constants.
  const PUBLISH_APPROVE_NONE = 0;
  const PUBLISH_APPROVE_ALL = 1;
  const PUBLISH_APPROVE_AUTHENTICATED = 2;

  /**
   * Comments filtering method.
   *
   * @var bool
   */
  private $useUrl = TRUE;

  /**
   * Number of comments to show by page.
   *
   * @var int
   */
  private $numberComments = 0;

  /**
   * Show Ajax more button on comments form.
   *
   * @var bool
   */
  private $moreButton = FALSE;

  /**
   * Comments filtering route name.
   *
   * @var string
   */
  private $routeName = '';

  /**
   * Comments filtering relative url.
   *
   * @var string
   */
  private $relativeUrl = '';

  /**
   * Build array method for route comments widget.
   *
   * @return array
   *   The comments widget render array.
   */
  public function build() {
    // Comments Form initial settings.
    $form_state = new FormState();
    $form_state->set('mode', 'add');
    $form_state->set('showMoreButton', $this->moreButton);
    $form_state->set('useUrl', $this->useUrl);
    $form_state->set('numberComments', $this->numberComments);
    $form_state->set('routeName', $this->routeName);
    $form_state->set('relativeUrl', $this->relativeUrl);
    $content = [];
    $content['conversation'] = $this->getComments(0, $this->numberComments);
    // For the first non-ajax call,
    // we set show more button to false if an empty comments page get.
    if ($content['conversation'] === FALSE) {
      $form_state->set('showMoreButton', FALSE);
    }
    // Finally get the comments form.
    $commentsForm = \Drupal::formBuilder()->buildForm('\Drupal\route_ajax_comments\Form\AjaxCommentsForm', $form_state);
    // Some meta for template.
    $meta['user'] = \Drupal::currentUser();
    return [
      '#theme' => 'route_ajax_comments_widget',
      '#params' => NULL,
      '#meta' => $meta,
      '#content' => $content,
      '#commentsForm' => $commentsForm,
      '#cache' => [
        'max-age' => 10,
      ],
    ];
  }

  /**
   * Gets conversation page based on widget settings.
   *
   * @param int $page
   *   The page number.
   *
   * @return array
   *   The comments page render array.
   */
  public function getComments($page) {
    // Return false flag on unlimited comments context.
    if ($this->numberComments == 0 && $page > 0) {
      // Return a false flag for a non possible context.
      return FALSE;
    }
    // Hack, no unlimited comments!
    if ($page == 0 && $this->numberComments == 0) {
      $this->numberComments = 500;
    }
    $filterField = $this->useUrl ? 'url' : 'route';
    $filterValue = $this->useUrl ? $this->relativeUrl : $this->routeName;
    $rangeStart = $page * $this->numberComments;
    // Get results by parameters.
    $query = \Drupal::entityQuery('route_comment_entity');
    // Only return the next newest articles.
    $comment_ids = $query->condition($filterField, $filterValue)
      ->condition('status', 1)
      ->sort('created', 'DESC')
      ->range($rangeStart, $this->numberComments)
      ->execute();
    $comments_entities = \Drupal::entityTypeManager()->getStorage('route_comment_entity')
      ->loadMultiple($comment_ids);
    $comments = [];
    // Load style for pictures.
    $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('thumbnail');
    // Load anonymous picture.
    $anonymousPicture = self::getAnonymousPicture();
    foreach ($comments_entities as $comment_entity) {
      $comment = [];
      // We made available the full entity for template custom usage.
      $comment['route_comment'] = $comment_entity;
      $comment['comment_body'] = $comment_entity->comment_body->value;
      $comment['created'] = $comment_entity->created->value;
      $comment['created_time_ago'] = \Drupal::service('date.formatter')->formatInterval(REQUEST_TIME - $comment_entity->created->value, 1);
      $owner = $comment_entity->getOwner();
      // We made owner available in template.
      $comment['owner'] = $owner;
      if ($owner && $owner->id() > 0 && !$owner->user_picture->isEmpty()) {
        // We'll use user tumbnails.
        $public_picture = $owner->get('user_picture')->entity->uri->value;
        $comment['picture_url'] = $style->buildUrl($public_picture);
      }
      elseif ($owner && $owner->id() == 0 && $anonymousPicture) {
        $comment['picture_url'] = $style->buildUrl($anonymousPicture);
      }
      elseif (!$owner) {
        $comment['comment_body'] = t('SPAM * This comment is no longer associated to any user! Locked.');
      }
      $comments[] = $comment;
    }
    if ($comments) {
      return [
        '#theme'      => 'route_ajax_comments_comments',
        '#comments'   => $comments,
        '#page'       => $page,
      ];
    }
    else {
      // Return flag for no comments.
      return FALSE;
    }
  }

  /**
   * Creates new route_comment_entity, based on context params.
   *
   * @param string $message
   *   The comment body to persist.
   *
   * @return bool
   *   The result.
   */
  public function saveComment($message) {
    try {
      // Try to create entity.
      $routeComment = RouteComment::create([
        'route' => $this->routeName,
        'url' => $this->relativeUrl,
        'host' => \Drupal::request()->getHost(),
        'name' => $this->randomToken(),
        'comment_body' => $message,
        'status' => self::getNewCommentStatus(),
        'user_id' => \Drupal::currentUser()->id(),
      ]);
      // Try to save new created entity.
      $routeComment->save();
      return TRUE;
    }
    catch (\Exception $e) {
      // Log possible error.
      \Drupal::logger('route_ajax_comments')->alert($e->getMessage());
      // Finally return false.
      return FALSE;
    }
  }

  /**
   * TODO: We can improve function with module permissions on user roles.
   *
   * @return bool
   *   The result.
   */
  public static function userCommentAllowed() {
    // Load global config.
    $globalCfg = \Drupal::config(static::SETTINGS);
    if ($globalCfg->get('allow_anonymous_comments') || \Drupal::currentUser()->id() > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Returns status for comments based on global settings policy.
   *
   * @return bool
   *   The result.
   */
  public static function getNewCommentStatus() {
    // Load global config.
    $globalCfg = \Drupal::config(static::SETTINGS);
    $uid = \Drupal::currentUser()->id();
    $publishMethod = $globalCfg->get('publish_method');
    // All comments must be approved.
    if ($publishMethod == self::PUBLISH_APPROVE_NONE) {
      return FALSE;
    }
    // Direct publishing for all users, including anonymous users.
    elseif ($publishMethod == self::PUBLISH_APPROVE_ALL) {
      return TRUE;
    }
    // Direct publishing only for Authenticated users and authenticated user.
    elseif ($uid > 0) {
      return TRUE;
    }
    // Direct publishing only for Authenticated users and user is anonymous.
    else {
      return FALSE;
    }
  }

  /**
   * Returns module global configuration settings.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config object for this module.
   */
  public static function getGlobalConfiguration() {
    return \Drupal::config(static::SETTINGS);
  }

  /**
   * Returns anonymous picture.
   *
   * @return string
   *   The image url path.
   */
  public static function getAnonymousPicture() {
    // Load global config.
    $globalCfg = \Drupal::config(static::SETTINGS);
    $uri = FALSE;
    $anonymousPicture = $globalCfg->get('anonymous_image');
    if ($anonymousPicture && isset($anonymousPicture[0])) {
      $file = File::load($anonymousPicture[0]);
      $uri = $file ? $file->uri->value : FALSE;
    }
    return $uri;
  }

  /**
   * Checks if a comment body is valid.
   *
   * TODO: Improve and extend cases.
   *
   * @param string $comment_body
   *   The comment body string.
   * @param array $message
   *   Referenced array with return control values.
   *
   * @return bool
   *   The result.
   */
  public static function isValidCommentBody($comment_body, array &$message) {
    if (!$comment_body) {
      $message['status'] = 0;
      $message['code'] = 1;
      $message['message'] = t('Empty comment body is not allowed');
      return FALSE;
    }
    // Load global config.
    $globalCfg = \Drupal::config(static::SETTINGS);
    // Check comment body length.
    $maxLength = $globalCfg->get('comment_body_max_length');
    if (strlen($comment_body) > $maxLength) {
      $message['status'] = 0;
      $message['code'] = 2;
      $message['message'] = t('Comment body length reached. Max allowed chars.') . $maxLength;
      return FALSE;
    }
    // PASSED.
    if (self::getNewCommentStatus()) {
      $message['status'] = 1;
      $message['code'] = 100;
      $message['message'] = t('Thanks for your comment!');
    }
    else {
      $message['status'] = 1;
      $message['code'] = 101;
      $message['message'] = t('Your comment was sent, but is pending approval!');
    }
    return TRUE;
  }

  /**
   * Gets private var.
   *
   * @return bool
   *   The value.
   */
  public function getUseUrl() {
    return $this->useUrl;
  }

  /**
   * Gets private var.
   *
   * @return int
   *   The value.
   */
  public function getNumberComments() {
    return $this->numberComments;
  }

  /**
   * Gets private var.
   *
   * @return bool
   *   The value.
   */
  public function getMoreButton() {
    return $this->moreButton;
  }

  /**
   * Sets private var.
   *
   * @param bool $useUrl
   *   The value.
   */
  public function setUseUrl($useUrl) {
    $this->useUrl = $useUrl;
  }

  /**
   * Sets private var.
   *
   * @param int $numberComments
   *   The value.
   */
  public function setNumberComments($numberComments) {
    $this->numberComments = $numberComments;
  }

  /**
   * Sets private var.
   *
   * @param bool $moreButton
   *   The value.
   */
  public function setMoreButton($moreButton) {
    $this->moreButton = $moreButton;
  }

  /**
   * Gets private var.
   *
   * @return string
   *   The value.
   */
  public function getRouteName() {
    return $this->routeName;
  }

  /**
   * Sets private var.
   *
   * @param string $routeName
   *   The value.
   */
  public function setRouteName($routeName) {
    $this->routeName = $routeName;
  }

  /**
   * Gets private var.
   *
   * @return string
   *   The value.
   */
  public function getRelativeUrl() {
    return $this->relativeUrl;
  }

  /**
   * Sets private var.
   *
   * @param string $relativeUrl
   *   The value.
   */
  public function setRelativeUrl($relativeUrl) {
    $this->relativeUrl = $relativeUrl;
  }

  /**
   * Stuff function connecting external API for getting quotes.
   *
   * You can update it with your favourite quotes API.
   * Thanks to https://kanye.rest/ It's cool!
   */
  public function getExternalQuote() {
    $uri = 'https://api.kanye.rest';
    try {
      $response = \Drupal::httpClient()->get($uri, ['headers' => ['Accept' => 'text/plain']]);
      $data = (string) $response->getBody();
      if (!isset($data['quote'])) {
        return $data;
      }
      else {
        return $data['quote'];
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }
  }

  /**
   * Getting a randomToken.
   *
   * Sometimes it's useful.
   *
   * @param int $length
   *   Length of token.
   *
   * @return string
   *   The result.
   */
  private function randomToken($length = 32) {
    if (!isset($length) || intval($length) <= 8) {
      $length = 32;
    }
    if (function_exists('random_bytes')) {
      return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
      return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    }
    if (function_exists('openssl_random_pseudo_bytes')) {
      return bin2hex(openssl_random_pseudo_bytes($length));
    }
  }

}
