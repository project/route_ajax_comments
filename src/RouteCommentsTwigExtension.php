<?php

namespace Drupal\route_ajax_comments;

use Drupal\route_ajax_comments\Controller\RouteAjaxComments;
use Drupal\Core\Url;

/**
 * Class DefaultService.
 *
 * @package Drupal\route_ajax_comments
 */
class RouteCommentsTwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('route_ajax_comments', [$this, 'routeAjaxComments']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'route_ajax_comments';
  }

  /**
   * Examples.
   * @code
   *   # Print widget using default configuration.
   *   {{ route_ajax_comments() }}
   *
   *   # Print block using custom configuration.
   *   {{ route_ajax_comments({use_url: true, more_button: false}) }}
   *
   * @param array $configuration
   *   The options array.
   *
   * @return array
   *   The render array.
   */
  public function routeAjaxComments(array $configuration = []) {

    $globalCfg = RouteAjaxComments::getGlobalConfiguration();

    $useUrl = isset($configuration['use_url']) ? $configuration['use_url'] : $globalCfg->get('use_url');
    $numberComments = isset($configuration['number_comments']) ? $configuration['number_comments'] : $globalCfg->get('number_comments');
    $moreButton = isset($configuration['more_button']) ? $configuration['more_button'] : $globalCfg->get('more_button');

    $racObject = new RouteAjaxComments();
    $racObject->setUseUrl($useUrl);
    $racObject->setNumberComments($numberComments);
    $racObject->setMoreButton($moreButton);
    // Sets current route name and relative url ,
    // loaded from the template this function was called.
    $racObject->setRouteName(\Drupal::routeMatch()->getRouteName());
    $racObject->setRelativeUrl(Url::fromRoute('<current>', [], ["relative" => TRUE])->toString());

    return $racObject->build();
  }

}
