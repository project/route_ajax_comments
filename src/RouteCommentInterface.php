<?php

namespace Drupal\route_ajax_comments;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Contact entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup route_ajax_comments
 */
interface RouteCommentInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
