<?php

namespace Drupal\route_ajax_comments\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\route_ajax_comments\Controller\RouteAjaxComments;
use Drupal\Core\Url;

/**
 * Provides an 'Ajax Route Comments' Block.
 *
 * @Block(
 *   id = "route_ajax_comments_block",
 *   admin_label = @Translation("Route Ajax Comments Block"),
 *   category = @Translation("Route Ajax Comments Block"),
 * )
 */
class RouteAjaxCommentsBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->getContent();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    // Load block configuration.
    $config = $this->getConfiguration();
    // Load module global configuration.
    $globalCfg = $globalCfg = RouteAjaxComments::getGlobalConfiguration();

    $form['use_url'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable url filtering.'),
      '#description' => $this->t('Enable field for using relative url as comment filtering instead of Route.'),
      '#default_value' => isset($config['use_url']) ? $config['use_url'] : $globalCfg->get('use_url'),
    ];

    $form['number_comments'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of comments.'),
      '#description' => $this->t('Number of comments to show. 0 for showing all comments. If value set, it wold be the number of initial comments to show, and the number of comments to load for each click on show more button.'),
      '#required' => TRUE,
      '#default_value' => isset($config['number_comments']) ? $config['number_comments'] : $globalCfg->get('number_comments'),
    ];

    $form['more_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show More Comments Button.'),
      '#description' => $this->t('Enable field for showing Ajax load more comments button.'),
      '#default_value' => isset($config['more_button']) ? $config['more_button'] : $globalCfg->get('more_button'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['use_url'] = $values['use_url'];
    $this->configuration['number_comments'] = $values['number_comments'];
    $this->configuration['more_button'] = $values['more_button'];
  }

  /**
   * Returns content render array.
   *
   * @return array
   *   The content render array.
   */
  private function getContent() {
    // Load block configuration.
    $config = $this->getConfiguration();

    $racObject = new RouteAjaxComments();
    $racObject->setUseUrl($config['use_url']);
    $racObject->setNumberComments($config['number_comments']);
    $racObject->setMoreButton($config['more_button']);
    // Sets current route name and relative url ,
    // loaded from the page this block was inserted.
    $racObject->setRouteName(\Drupal::routeMatch()->getRouteName());
    $racObject->setRelativeUrl(Url::fromRoute('<current>', [], ["relative" => TRUE])->toString());

    return $racObject->build();
  }

}
