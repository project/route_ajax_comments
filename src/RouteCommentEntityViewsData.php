<?php

namespace Drupal\route_ajax_comments;

use Drupal\views\EntityViewsData;

/**
 * Provides improvements to core's generic views integration for entities.
 */
class RouteCommentEntityViewsData extends EntityViewsData {

}
