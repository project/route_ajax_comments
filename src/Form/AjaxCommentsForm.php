<?php

namespace Drupal\route_ajax_comments\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\route_ajax_comments\Controller\RouteAjaxComments;
use Drupal\Core\Url;

/**
 * Form control for Comments Widget.
 *
 * Form with two operation modes 'add comment' mode and 'send comment' mode.
 *
 * We need to define private vars as state variables filled in requests
 * for using form route directly in ajax calls.
 * Most of the concepts taken from:
 * https://www.drupal.org/project/drupal/issues/2504115
 *
 * @see \Drupal\Core\Form\FormBase
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class AjaxCommentsForm extends FormBase {

  /**
   * Form flow mode.
   *
   * @var string
   */
  private $mode = NULL;

  /**
   * Last Message to show.
   *
   * @var string
   */
  private $message = NULL;

  /**
   * Last error status.
   *
   * @var bool
   */
  private $error = NULL;

  /**
   * MoreButton show state.
   *
   * @var bool
   */
  private $moreButton = NULL;

  /**
   * Should use routes or url as comments filtering?
   *
   * @var bool
   */
  private $useUrl = NULL;

  /**
   * Number of comments per page.
   *
   * @var int
   */
  private $numberComments = NULL;

  /**
   * Route Name for filtering.
   *
   * @var string
   */
  private $routeName = NULL;

  /**
   * Relative URL for filtering.
   *
   * @var string
   */
  private $relativeUrl = NULL;

  /**
   * Number of comments page to show.
   *
   * @var int
   */
  private $page = NULL;


  /**
   * Temp store variable.
   *
   * We need this variable as a temporal store between the submit and
   * callback actions for the showMoreButton action.
   *
   * @var int
   */
  private $lastConversationPage = NULL;

  /**
   * Build Form method.
   *
   * Built in function of the
   * request query string (private properties).
   *
   * @see getFormCustomParams
   * @see getFormExtraQuery
   *
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Load custom params.
    $this->getFormCustomParams($form_state);

    $form['#tree'] = TRUE;

    $form['add_comment_fieldset'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="add-comment-fieldset-wrapper"',
      '#suffix' => '</div>',
    ];

    $form['add_comment_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    if (RouteAjaxComments::userCommentAllowed()) {

      if ($this->mode == 'send') {

        $form['add_comment_fieldset']['comment'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Your Comment'),
          '#attributes' => ['class' => ['route-ajax-comments-comment-control']],
          '#default_value' => '',
          '#weight' => 0,
        ];

        $form['add_comment_fieldset']['actions']['send'] = [
          '#type' => 'submit',
          '#value' => $this->t('Send'),
          '#attributes' => ['class' => ['route-ajax-comments-send-button']],
          '#submit' => ['::sendComment'],
          '#ajax' => [
            'callback' => '::sendCommentCallback',
            'url' => Url::fromRoute('route_ajax_comments.comments_form'),
            'wrapper' => 'add-comment-fieldset-wrapper',
            'options' => [
              'query' => [
                FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
              ],
            ],
          ],
        ];

      }
      elseif ($this->mode == 'end') {

        $lastMessageClass = $this->error ? "add-comment-last-message-error" : "add-comment-last-message-success";

        $form['add_comment_fieldset']['message'] = [
          '#type' => 'item',
          '#markup' => $this->message,
          '#prefix' => '<div id="add-comment-last-message" class="' . $lastMessageClass . '"',
          '#suffix' => '</div>',
        ];

        $form['add_comment_fieldset']['actions']['add'] = [
          '#type' => 'submit',
          '#value' => $this->t('Add Comment'),
          '#attributes' => ['class' => ['route-ajax-comments-add-button']],
          '#submit' => ['::addComment'],
          '#ajax' => [
            'callback' => '::addCommentCallback',
            'url' => Url::fromRoute('route_ajax_comments.comments_form'),
            'wrapper' => 'add-comment-fieldset-wrapper',
            'options' => [
              'query' => [
                FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
              ],
            ],
          ],
        ];

      }
      else {

        $form['add_comment_fieldset']['actions']['add'] = [
          '#type' => 'submit',
          '#value' => $this->t('Add Comment'),
          '#attributes' => ['class' => ['route-ajax-comments-add-button']],
          '#submit' => ['::addComment'],
          '#ajax' => [
            'callback' => '::addCommentCallback',
            'url' => Url::fromRoute('route_ajax_comments.comments_form'),
            'wrapper' => 'add-comment-fieldset-wrapper',
            'options' => [
              'query' => [
                FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
              ],
            ],
          ],
        ];

      }

    }
    // The end mode checks for a last action message and puts it in the form.
    elseif ($this->mode == 'end') {
      $lastMessageClass = $this->error ? "add-comment-last-message-error" : "add-comment-last-message-success";

      $form['add_comment_fieldset']['message'] = [
        '#type' => 'item',
        '#markup' => $this->message,
        '#prefix' => '<div id="add-comment-last-message" class="' . $lastMessageClass . '"',
        '#suffix' => '</div>',
      ];

    }

    if ($this->moreButton) {

      $form['add_comment_fieldset']['actions']['show_more_comments'] = [
        '#type' => 'submit',
        '#value' => $this->t('Show More'),
        '#attributes' => ['class' => ['route-ajax-comments-show-more-button']],
        '#submit' => ['::showMoreComments'],
        '#ajax' => [
          'callback' => '::showMoreCommentsCallback',
          'url' => Url::fromRoute('route_ajax_comments.comments_form'),
          'wrapper' => 'add-comment-fieldset-wrapper',
          'options' => [
            'query' => [
              FormBuilderInterface::AJAX_FORM_REQUEST => TRUE,
            ],
          ],
        ],
      ];

    }

    // Add extra query strings for actions.
    foreach ($form['add_comment_fieldset']['actions'] as &$formAction) {
      // Prevent if is not a control.
      if (!is_array($formAction)) {
        continue;
      }
      $formAction['#ajax']['options']['query'] += $this->getFormExtraQuery();
    }

    return $form;
  }

  /**
   * Prepares ajax request query from class private variables.
   *
   * @return array
   *   The array containing variables for an ajax request query string.
   */
  private function getFormExtraQuery() {

    $queryString = [];
    $queryString['showMoreButton'] = $this->moreButton;
    $queryString['routeName'] = $this->routeName;
    $queryString['relativeUrl'] = $this->relativeUrl;
    $queryString['useUrl'] = $this->useUrl;
    $queryString['numberComments'] = $this->numberComments;
    $queryString['page'] = $this->page;
    return $queryString;
  }

  /**
   * Fills class private vars (pseudo form state).
   *
   * First we should try to recover values from form state.
   * If it's not possible to recover it, we should try to
   * recover from query string.
   *
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @result bool
   *   Always true if there is not a PHP error.
   */
  private function getFormCustomParams(FormStateInterface $form_state) {

    $this->mode = $form_state->get('mode');
    $this->message = $form_state->get('message');
    $this->error = $form_state->get('error');
    $this->moreButton = $form_state->get('showMoreButton');
    $this->useUrl = $form_state->get('useUrl');
    $this->numberComments = $form_state->get('numberComments');
    $this->routeName = $form_state->get('routeName');
    $this->relativeUrl = $form_state->get('relativeUrl');
    $this->page = $form_state->get('page');

    $requestQuery = \Drupal::request()->query->all();

    // Try to get value from query string,
    // if is an Ajax Call from self form.
    if ($this->moreButton === NULL) {
      $this->moreButton = isset($requestQuery['showMoreButton']) ? $requestQuery['showMoreButton'] : NULL;
    }

    if ($this->routeName === NULL) {
      $this->routeName = isset($requestQuery['routeName']) ? $requestQuery['routeName'] : NULL;
    }

    if ($this->relativeUrl === NULL) {
      $this->relativeUrl = isset($requestQuery['relativeUrl']) ? $requestQuery['relativeUrl'] : NULL;
    }

    if ($this->useUrl === NULL) {
      $this->useUrl = isset($requestQuery['useUrl']) ? $requestQuery['useUrl'] : NULL;
    }

    if ($this->numberComments === NULL) {
      $this->numberComments = isset($requestQuery['numberComments']) ? $requestQuery['numberComments'] : NULL;
    }

    if ($this->page === NULL) {
      $this->page = isset($requestQuery['page']) ? $requestQuery['page'] : NULL;
    }
    // Default success.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ajax_add_comment';
  }

  /**
   * Callback for add comment.
   */
  public function addCommentCallback(array &$form, FormStateInterface $form_state) {
    // Nothing, only rebuild form.
    return $form['add_comment_fieldset'];
  }

  /**
   * Callback for send comment button.
   */
  public function sendCommentCallback(array &$form, FormStateInterface $form_state) {

    $error = $form_state->get('error');
    $response = new AjaxResponse();

    if (!$error) {
      $comment = $form_state->getValue(['add_comment_fieldset', 'comment']);
      $rac = new RouteAjaxComments();
      $rac->setRouteName($this->routeName);
      $rac->setRelativeUrl($this->relativeUrl);
      $rac->saveComment($comment);
    }

    // Render form as it is in ajax command.
    $response->addCommand(new HtmlCommand('#route-ajax-comments-form-wrapper', $form));

    return $response;
  }

  /**
   * Callback for show More comments Button.
   */
  public function showMoreCommentsCallback(array &$form, FormStateInterface $form_state) {

    $response = new AjaxResponse();

    if ($this->lastConversationPage !== FALSE) {
      $response->addCommand(new AppendCommand('#route-ajax-comments-conversation-wrapper', $this->lastConversationPage));
      // Animate after appending the element.
      $response->addCommand(new InvokeCommand(NULL, 'showMoreComments', ['#conversation-page-' . $this->page]));
    }
    else {
      // Render form as it is in ajax command.
      $response->addCommand(new HtmlCommand('#route-ajax-comments-form-wrapper', $form));
    }

    return $response;
  }

  /**
   * Submit handler for the "add comment" button.
   *
   * Modify mode to 'send' and causes a rebuild.
   */
  public function addComment(array &$form, FormStateInterface $form_state) {
    $form_state->set('mode', 'send');
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "send comment" button.
   *
   * Modify mode to 'add' and causes a rebuild.
   */
  public function sendComment(array &$form, FormStateInterface $form_state) {
    $form_state->set('mode', 'end');
    $comment = $form_state->getValue(['add_comment_fieldset', 'comment']);
    $message = [];

    if (RouteAjaxComments::isValidCommentBody($comment, $message)) {
      $form_state->set('message', $message['message']);
      $form_state->set('error', FALSE);
    }
    else {
      $form_state->set('message', 'Error - ' . $message['message']);
      $form_state->set('error', TRUE);
    }

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "show more comments" button.
   *
   * Gets the new conversation page and stores , if available,
   * in lastConversationPage property.
   * Controls the form flow if there are no more pages to show.
   *
   * causes a form rebuild.
   */
  public function showMoreComments(array &$form, FormStateInterface $form_state) {
    // Have to recalculate params again for using in
    // RouteAjaxComment new instance.
    $this->getFormCustomParams($form_state);

    $form_state->set('mode', 'add');
    $page = !$form_state->get('page') ? 1 : $form_state->get('page') + 1;
    $form_state->set('page', $page);

    // We need to get the results of the new conversation page in the
    // submit action and not in the callback
    // because we want to disable showMoreButton when no more
    // pages in the conversation.
    // If we do it in the callback, we can't propagate in the query
    // string the new showMoreButton value.
    $rac = new RouteAjaxComments();
    $rac->setRouteName($this->routeName);
    $rac->setRelativeUrl($this->relativeUrl);
    $rac->setUseUrl($this->useUrl);
    $rac->setNumberComments($this->numberComments);
    $output = $rac->getComments($page);

    // Check for a new conversation page.
    if ($output === FALSE) {
      $form_state->set('showMoreButton', FALSE);
      $form_state->set('mode', 'end');
      $form_state->set('message', $this->t('Sorry! No more comments to show.'));
      $form_state->set('error', TRUE);
    }
    // Always set last response, including false
    // flag for processing actions in the callback.
    $this->lastConversationPage = $output;

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Nothing.
  }

}
