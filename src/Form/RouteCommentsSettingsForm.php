<?php

namespace Drupal\route_ajax_comments\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings fot Route Ajax Comments general.
 *
 * @ingroup route_ajax_comments
 */
class RouteCommentsSettingsForm extends ConfigFormBase {

  const SETTINGS = 'route_ajax_comments.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'route_comments_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['route_comments_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Route Ajax Comments "GLOBAL" Settings.'),
      '#prefix' => '<div id="route-comments-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['route_comments_settings']['anonymous_comments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow Anonymous Comments.'),
      '#description' => $this->t('Allow anonymous users posting comments. It can produce lots of SPAM.'),
      '#default_value' => $config->get('allow_anonymous_comments'),
    ];

    // Radios.
    $form['route_comments_settings']['publish_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Comment Publishing Method.'),
      '#options' => [
        0 => $this->t('All comments must be approved.'),
        1 => $this->t('Direct publishing for all users, including anonymous users.'),
        2 => $this->t('Direct publishing only for Authenticated users.'),
      ],
      '#default_value' => $config->get('publish_method'),
      '#description' => $this->t('Select one from allowed publish methods. Be aware with anonymous option, it could produce lots of SPAM content.'),
      '#required' => TRUE,
    ];

    $form['route_comments_settings_defaults'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Route Ajax Comments "Default Widget" Settings.'),
      '#description' => $this->t('You can set a custom configuration for each widget you are using: block or template.'),
      '#prefix' => '<div id="route-comments-widget-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['route_comments_settings_defaults']['use_url'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable URL filtering.'),
      '#description' => $this->t('Enable field for using relative url as comment filtering instead of Route.'),
      '#default_value' => $config->get('use_url'),
    ];

    $form['route_comments_settings_defaults']['number_comments'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of comments.'),
      '#description' => $this->t('Number of comments to show. 0 for showing all comments. If value set, it wold be the number of initial comments to show, and the number of comments to load for each click on show more button.'),
      '#required' => TRUE,
      '#default_value' => $config->get('number_comments'),
    ];

    $form['route_comments_settings_defaults']['more_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show More Comments Button.'),
      '#description' => $this->t('Enable field for showing Ajax load more comments button.'),
      '#default_value' => $config->get('more_button'),
    ];

    $form['route_comments_settings_limits'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Route Ajax Comments Limits.'),
      '#description' => $this->t('Route Ajax Comments Limits.'),
      '#prefix' => '<div id="route-comments-limits-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['route_comments_settings_limits']['comment_body_max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Comment Body Max Length.'),
      '#description' => $this->t('User posting message will get a invalid error message if comment length is greater than this (in characters).'),
      '#required' => TRUE,
      '#default_value' => $config->get('comment_body_max_length'),
    ];

    $form['route_comments_settings_styles'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Route Ajax Comments "Default Widget Style" Settings.'),
      '#description' => $this->t('Appearance settings.'),
      '#prefix' => '<div id="route-comments-widget-style-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['route_comments_settings_styles']['anonymous_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Anonymous picture'),
      '#upload_validators' => [
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [25600000],
      ],
      '#theme' => 'image_widget',
      '#preview_image_style' => 'thumbnail',
      '#upload_location' => 'public://pictures/',
      '#required' => FALSE,
      '#default_value' => $config->get('anonymous_image'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('allow_anonymous_comments', $form_state->getValue('anonymous_comments'))
      ->set('publish_method', $form_state->getValue('publish_method'))
      ->set('use_url', $form_state->getValue('use_url'))
      ->set('number_comments', $form_state->getValue('number_comments'))
      ->set('more_button', $form_state->getValue('more_button'))
      ->set('comment_body_max_length', $form_state->getValue('comment_body_max_length'))
      ->set('anonymous_image', $form_state->getValue('anonymous_image'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
